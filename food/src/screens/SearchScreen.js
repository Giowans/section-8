import React, {useState} from 'react';
import {View, StyleSheet, Text} from 'react-native';
import SearchBar from '../components/SearchBar';

const SearchScreen = () =>
{
    const [text, setText] = useState('');
    return (
        <View>
            <SearchBar 
                term = {text} 
                onTermChange = {(newText) => setText(newText)}
                onTermSubmited = {(newText) => {
                    
                }}
            />
        </View>
    );
};

const styles = StyleSheet.create({});

export default SearchScreen;