# Section 8: ¡Comenzando una App desde Cero!

¡Por fin en esta sección hemos empezado a aplicar todo lo aprendido anteriormente! Desde crear componentes hasta aplicar estilos correctamente a una apliación real (o sea, que tenga un _MockUp_ diseñado previamente y que esta aplicado a una problemática real).

Lo primero que se hiz en esta sección fue usar el cliente de **Expo** para crear nuestro proyecto desde 0: instalando varias dependencias y fomandonos un _blank template_ (que a final de cuentas terminamos borrando xD). Tambien creamos nuestro _stackNavigator_ desde cero, cosa que es muy importante.

Aunque en esta sección simplemente logramos crear el componente de la barra de búsqueda estoy muy emocionado con el resultado :3

### Nota:
- Creo que lo más complicado fue haber creado el proyecto el proyecto con el cliente de Expo, ya que mi _Node Package Manager_ me dio muchos conflictos, tanto así que despúes de un rato de haber investigado, opte mejor por instalar el cliente de expo (`npm install -g expo-cli`) y despues instalar y usar `yarn` para generarlo.
- El comando para instalar **yarn** fue similar: `npm install -g yarn`.

